//Using requestAnimationFrame, animate the background-color of the body so that it changes from black 
//rgb(0, 0, 0) to white rgb(255, 255, 255).  It should increment by 1every frame.
//Once the background-color is rgb(255, 255, 255), should not call requestAnimationFrame again.
//It should take approximately 4-5 seconds for the background to animate from black to white.

let colorNumber = 0; 
let textColor = 255; 
const animate = function () {
    colorNumber++; 
    textColor--;  
    if (colorNumber <= 255){
        document.body.style.backgroundColor = "rgb(" + colorNumber + ", " + colorNumber + ", " + colorNumber + ")"; 
        document.getElementById('title').style.color = "rgb(" + textColor + ", " + textColor + ", " + textColor + ")"; 
        requestAnimationFrame(animate); 
    }
}

requestAnimationFrame(animate); 