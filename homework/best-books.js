// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
//A) Takes the user submission for year, month, and date.  You can assume that the input is valid.
//B) Calls the New York Times API to find the best selling hardcover fiction books for that date
//C) Displays the following items for the first 5 books:
//Title
//Author
//Description



function updateList(event){
    const year = document.getElementById("year").value; 
    const month = document.getElementById("month").value; 
    const inputDate = document.getElementById("date").value; 
    date = year + "-" + month + "-" + inputDate;
    console.log(date); 
    populateList(date);
    event.preventDefault()
}

function getUrl(date){
    const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/'+ date +'/hardcover-fiction.json';
    const url = `${BASE_URL}?api-key=${API_KEY}`;
    return url; 
}

document.getElementById('submit').addEventListener("click", updateList); 

// Use fetch() to make the request to the API
function populateList(date) {
    let url = getUrl(date);
    fetch(url).then(function(result) {
        return result.json();
    }).then(displayBooks);
}

function displayBooks(json) {
    console.log(json);

    results = json.results; 
    books = results.books; 
    console.log(books); 
    booksContainer = document.getElementById('books-container'); 

    for (i = 0; i < 5; i++ ){
        title = books[i].title; 
        author = books[i].author; 
        description = books[i].description; 
        bookImage = books[i].book_image; 
        let image = document.createElement("IMG"); 
        image.setAttribute('src', bookImage); 
        booksContainer.appendChild(image); 
        let createTitleP = document.createElement("P"); 
        let displayTitle = document.createTextNode(title); 
        let authorDisplay = document.createElement("SMALL")
        let displayAuthor = document.createTextNode(author); 
        let descriptionP = document.createElement("P"); 
        let displayDescription = document.createTextNode(description); 
        createTitleP.appendChild(displayTitle); 
        authorDisplay.appendChild(displayAuthor); 
        descriptionP.appendChild(displayDescription);
        booksContainer.append(createTitleP, authorDisplay, descriptionP); 

    }

}

//document.body.addEventListener("load", populateList('current')); 




