let thePromise =  new Promise(function(resolve, reject){
    setTimeout(function(){
        if (Math.random() > 0.5){
            resolve('sucess'); 
        }else{
            reject(new Error('fail')); 
        }
    }, 1000); 
 }); 


thePromise.then(function(value){
    console.log(value); 
 }).catch(function(error){
    console.log(error.message); 
 }).then(function(value){
    console.log('complete'); 
 }); 
 
 

