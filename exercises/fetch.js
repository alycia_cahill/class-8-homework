const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';

const url = `${BASE_URL}?q=cars&api-key=AZPYcdu7JHCd1J8GP0pvwnK5AFxb998s`;

fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);

    let article = responseJson.response.docs[0];
    console.log(article);

    const mainHeadline = article.headline.main;
    document.getElementById('article-title').innerText = mainHeadline;

    const mainSnippet = article.snippet; 
    document.getElementById('article-snippet').innerText = mainSnippet; 

    const articleLink = article.web_url; 
    document.getElementById('article-link').href = articleLink; 

    const articleDate = article.pub_date; 
    document.getElementById('article-date').innerText = articleDate; 
    
    if (article.multimedia.length > 0) {
      const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
      document.getElementById('article-img').src = imgUrl;
    }

  });
